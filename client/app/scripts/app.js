'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # appApp
 *
 * Main module of the application.
 */
angular
  .module('ferariApp', [
  	'countTo',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngTouch',
    /*'ngSanitize',*/
    'ui-leaflet',
    'ui.router',
    'ngMdIcons',
    'ui.grid', 
    'ui.grid.selection',
    'ui.grid.cellNav',
    'ui.grid.pagination',
    'ui.grid.resizeColumns',
    'ui.grid.exporter',
   /* 'ngCsv',*/
    'angularMoment',
    angularDragula(angular)
  ])
  .config( function ($stateProvider, $urlRouterProvider, $locationProvider, $mdGestureProvider){
    $stateProvider
      .state('home', {
        url:'',
        abstract: true,
        templateUrl: 'views/home.html',
        controller: 'MainController'
      })
      .state('home.dashboard', {
        url: '/dashboard',
        templateUrl: 'views/dashboard.html',
        data: {
          title: 'Dashboard'
        }
      })
      .state('home.graphs', {
        url: '/graphs',
        templateUrl: 'views/partials/graphs.html',
        data: {
          title: 'Graphs'
        }
      })
      .state('home.table', {
        url: '/table',
        templateUrl: 'views/partials/widgets/table-widget.html',
        data: {
          title: 'Table'
        }
      })
      .state('home.map', {
        url: '/map',
        templateUrl: 'views/partials/widgets/map-widget.html',
        data: {
          title: 'Map'
        }
      })
      .state('detail', {
        url: 'detail/:_id/:calling_number/:Name',
        templateUrl: 'views/detail.html'
      });

    // For any unmatched url, send to /
    $urlRouterProvider.otherwise('/dashboard');
    $mdGestureProvider.skipClickHijack();
    //remove the hashtag from URL
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  });
