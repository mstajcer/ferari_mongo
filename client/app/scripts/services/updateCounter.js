'use strict';

/**
 * @ngdoc function
 * @name ferariApp.service:UpdateCounter
 * @description: Service for sharing data between TableCtrl and EventCounterCtrl
 * # UpdateCounter
 * Service of the ferariApp
 */

angular.module('ferariApp')
    .factory('UpdateCounter', ['socketio', function (socketio) {
        var countToTotal  = { count: 0 };

        socketio.on('message', function (msg) {
            countToTotal.count += 1;
            console.log(countToTotal );
        });
        return countToTotal ;
}]);