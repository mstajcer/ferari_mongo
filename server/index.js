var express = require('express');

var app = express();

app.use('/', express.static('../client/app'));
app.use('/bower_components', express.static('../client/bower_components/'));

var server = require('http').Server(app);
var io = require('socket.io').listen(server);

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
    //ObjectID = require('mongodb').ObjectID;
	//var BSON = require('mongodb').BSONPure;
var ObjectId = require('mongodb').ObjectID;

//MongoDB Connection URL
var url = 'mongodb://localhost:27017/ferari';

// Use connect method to connect to the Server 
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected to the MongoDB server");

  	//Get all events from events collection
  	app.get('/events', function(req, res){
		db.collection('events').find({}).sort({OccurrenceTime: -1}).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			} 
		});
	});

	//Get number of events in last hour
  	app.get('/onehour', function(req, res){
		db.collection('events').aggregate([
			{
				$match: {
					$and: [
						{
							OccurrenceTime: {
								 $gte: new Date().getTime() - 1000 * 60 * 60
							}
						},
						{
							OccurrenceTime: {
								$lte: new Date().getTime()
							}
						}
					]
				}
			},
			{
				$group: {
					_id : '$Name', NO : {$sum: 1 }
				}
			},
			{
				$project: { 
					_id:0, Name: '$_id', NO: 1
				}
			}
		]).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			}
		});
	});

  	//Get number of events in last six hours
	app.get('/sixhours', function(req, res){
		db.collection('events').aggregate([
			{
				$match: {
					$and: [
						{
							OccurrenceTime: {
								 $gte: new Date().getTime() - 6000 * 60 * 60
							}
						},
						{
							OccurrenceTime: {
								$lte: new Date().getTime()
							}
						}
					]
				}
			},
			{
				$group: {
					_id : '$Name', NO : {$sum: 1 }
				}
			},
			{
				$project: { 
					_id:0, Name: '$_id', NO: 1
				}
			}
		]).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			}
		});
	});

	//Get number of events in past day
	app.get('/24hours', function(req, res){
		db.collection('events').aggregate([
			{
				$match: {
					$and: [
						{
							OccurrenceTime: {
								 $gte: new Date().getTime() - 24000 * 60 * 60
							}
						},
						{
							OccurrenceTime: {
								$lte: new Date().getTime()
							}
						}
					]
				}
			},
			{
				$group: {
					_id : '$Name', NO : {$sum: 1 }
				}
			},
			{
				$project: { 
					_id:0, Name: '$_id', NO: 1
				}
			}
		]).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			}
		});
	});


	//Get subscriber data from subscriber collection
  	app.get('/subscriber/:calling_number', function(req, res){
  		var calling_number = req.params.calling_number;
  		console.log(calling_number);
		db.collection('subscriber').findOne({calling_number: calling_number},function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			} 
		});
	});

	//Get detail event data from events collection
  	app.get('/derived/:_id', function(req, res){
  		var calling_number = req.params.calling_number;
  		var _id = req.params._id;
  		console.log(calling_number);
  		var name = req.params.Name;
  		console.log(_id);

  		var query = [	
  			{   $unwind: "$call_start_dates" },
  			{
				$match: {
					"_id": new ObjectId(_id)
				}
			},
            {   
            	$group: {
                    _id: "$_id",
                    call_start_dates: { $addToSet: "$call_start_dates" },
                    called_numbers: { $first: "$called_numbers" },
                    other_party_tel_numbers: { $first: "$other_party_tel_numbers" },
                    conversation_durations: { $first: "$conversation_durations" }
                }
            },
            {
                $unwind: {
                    path: "$call_start_dates",
                    includeArrayIndex: "idx"
                }
            },
            { 
                $project: { 
                    _id: 1, 
                    call_start_dates: 1, 
                    called_numbers: { 
                        $arrayElemAt: ["$called_numbers", "$idx"] 
                    },
                    other_party_tel_numbers: { 
                        $arrayElemAt: ["$other_party_tel_numbers", "$idx"] 
                    }, 
                    conversation_durations: { 
                        $arrayElemAt: ["$conversation_durations", "$idx"]
                    }
                }
            }
		];

		db.collection('events').aggregate(query).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			} 
		});
	});

	//TO DO - napraviti match još po call_start_date-u
	app.get('/longcallatnight/:calling_number', function(req, res){
  		var calling_number = req.params.calling_number;
  		console.log(calling_number);

		db.collection('events').findOne({calling_number: calling_number},function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			} 
		});
	});


	//API for mostfrequent customer
	app.get('/mostfrequent', function(req, res){

  		var query = [
  			{   
  				$unwind: '$called_numbers'
  			}, 
		    {
		        $group: {
		            _id : '$called_numbers',  
		            no_occurence: {$sum: 1 },
		        }
		    },
		    {
		        $project: { 
		            _id: 0, called_number: '$_id', no_occurence: 1
		        }
		    },
		    { 
		    	$sort : { 
		    		no_occurence : -1,
		    	} 
		    }
  		];

		db.collection('events').aggregate(query).toArray(function(err, docs) {
			if (err) {
				assert.equal(null);
			}
			else {
				console.log(docs);
		    	res.json(docs);
			} 
		});
	});

	//API for peak/off peak graph
	


	//TO DO - API for getting coordinate -> generate random CELL_ID in new collection and join with coordinates

}); //end of Mongo connection


io.sockets.on('connection', function(socket){

    socket.on("fraudEvents", function(message) {
    	io.emit("message",  message);
    	//console.log(message);
    });

    //transform realtime data for onehour chart - only DERIVED_EVENT_NAME and NO needed
    socket.on("fraudEvents", function(lasthour,callback) {
    	//var parseData = JSON.parse(lasthour);
    	//var data = lasthour;
    	//console.log(data);
    	//var grouped = [];

		/*data.forEach(function (a) {
		    if (!this[a.Name]) {
		        this[a.Name] = { Name: a.Name, NO: 0 };
		        grouped.unshift(this[a.Name]);
		    }
		    this[a.Name].NO++;
		}, Object.create(null));

		console.log(grouped);*/
		callback("");
    	io.emit("lasthour", " [ " + lasthour + "]");
    	
	});

    socket.on('disconnect', function() {
        //subscribe.quit();
    });
});

server.listen(3000, function () {
    'use strict';
});